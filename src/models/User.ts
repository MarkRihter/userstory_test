export default interface User {
  login: string;
  name?: string | null | undefined;
  email?: string | null | undefined;
  id: number;
  avatar_url: string;
}

export function isUser(obj: any): obj is User {
	return typeof obj.id === 'number' &&
  typeof obj.avatar_url === 'string' &&
  typeof obj.login === 'string' &&
  ( typeof obj.name === 'string' || 
    obj.name === null || 
    obj.name === undefined 
  ) && ( typeof obj.email === 'string' || 
    obj.email === null || 
    obj.email === undefined 
	)
}

export function isArrayOfUsers(obj: any[]): obj is User[] {
	return obj.every(item => isUser(item))
}