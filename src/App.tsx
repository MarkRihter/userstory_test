import React from 'react'
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
} from 'react-router-dom'

import MainPage from './pages/MainPage'
import UserPage from './pages/UserPage'

const App = () => {
	return (
		<Router>
			<Switch>
				<Route path="/user/:login">
					<UserPage />
				</Route>
				<Redirect from="/:smth" to="/"/>
				<Route exact path="/">
					<MainPage />
				</Route>
			</Switch>
		</Router>
	)
}

export default App
