import { request } from '@octokit/request'

export default request.defaults({
	headers: {
		authorization: `token ${process.env.REACT_APP_GITHUB_TOKEN}`,
	},
})