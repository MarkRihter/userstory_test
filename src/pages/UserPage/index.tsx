import React, { FC, useEffect, useState } from 'react'
import Container from '@material-ui/core/Container'
import Skeleton from '@material-ui/lab/Skeleton'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import { useParams } from 'react-router-dom'

import octokit from '../../utils/octokit'
import User from '../../models/User'

import './index.scss'

const UserPage: FC = () => {
	const { login } = useParams<{ login: string }>()
	const [isLoading, setLoadingState] = useState(true)
	const [user, setUser] = useState<User | null>(null)
	const [error, setError] = useState<Error | null>(null)

	useEffect(() => {
		const getUser = async () => {
			try {
				const response = await octokit(`GET /users/${login}`)
				setLoadingState(false)
				setUser(response.data)
			} catch (error) {
				setError(error)
				setLoadingState(false)
			}
		}
	
		getUser()
		
	}, [])

	const renderUser = () => <>
		<img src={user?.avatar_url} width={200} height={200} className='avatar'/>
		<div className='description'>
			<h2>{user?.name || user?.login}</h2>
			<h4>{user?.email || 'no email available'}</h4>
		</div>
	</>

	const renderSkeleton = () => <>
		<Skeleton variant="rect" width={200} height={200} className='avatar'/>
		<div className='description'>
			<Typography variant="h3">
				<Skeleton />
			</Typography>
			<Typography variant="h5">
				<Skeleton />
			</Typography>
		</div>
	</>

	const renderError = () => <>
		<img src={user?.avatar_url} width={200} height={200} className='avatar'/>
		<div className='description'>
			<h2>{error?.message}</h2>
		</div>
	</>

	return <Container>
		<div className="user-page">
			<Paper className='user-container' elevation={3}>
				{ error ? renderError() : isLoading ? renderSkeleton() : renderUser()}
			</Paper>
		</div>
	</Container>
}

export default UserPage