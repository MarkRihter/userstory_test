import React, { FC, useEffect } from 'react'

import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import Avatar from '@material-ui/core/Avatar'
import Skeleton from '@material-ui/lab/Skeleton'
import { Link } from 'react-router-dom'

import { useAppSelector, useAppDispatch } from '../../store/hooks'
import { getUsersAction } from './slice'

import './index.scss'

const MainPage: FC = () => {

	const users = useAppSelector(state => state.users)
	const isLoading = useAppSelector(state => state.isLoading)
	const dispatch = useAppDispatch()

	useEffect(() => {
		dispatch(getUsersAction())
	}, [])

	const renderSkeleten = () => Array.from({length: 10}).map((_, index) => <Paper key={index} className='user-card' elevation={3}>
		<Skeleton variant="circle" width={40} height={40} />
		<Skeleton variant="text" className='caption'/>
	</Paper>)

	const renderUsers = () => users.map(user => <Link key={user.id} to={`/user/${user.login}`}>
		<Paper className='user-card' elevation={3}>
			<Avatar alt={user.login} src={user.avatar_url} />
			<p className='caption'>{user.name || user.login}</p>
		</Paper>
	</Link>)

	return <Container>
		<div className="main-page">
			{isLoading === 'loading' ? renderSkeleten() : renderUsers() }
		</div>
	</Container> 
}

export default MainPage
