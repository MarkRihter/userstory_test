import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import octokit from '../../utils/octokit'
import User, {isArrayOfUsers} from '../../models/User'

interface CounterState {
  users: User[];
  error: any,
  isLoading: 'loading' | 'loaded';
}

const initialState: CounterState = {
	users: [],
	error: null,
	isLoading: 'loaded',
}

export const getUsersAction = createAsyncThunk(
	'main/getUsers',
	async (amount?: number) => {
		const users = await octokit('GET /users', {
			per_page: amount || 10
		})
		return isArrayOfUsers(users.data) ? users.data : []
	}
)

export const counterSlice = createSlice({
	name: 'main',
	initialState,
	reducers: {
	},
	extraReducers: (builder) => {
		builder
			.addCase(getUsersAction.pending, (state) => {
				state.isLoading = 'loading'
				state.error = null
				state.users = []
			})
			.addCase(getUsersAction.rejected, (state, err) => {
				state.isLoading = 'loaded'
				state.error = err.error
			})
			.addCase(getUsersAction.fulfilled, (state, action) => {
				state.isLoading = 'loaded'
				state.users = action.payload
			})
	},
})

export default counterSlice.reducer
